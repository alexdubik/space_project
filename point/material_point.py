import math


class MaterialPoint:

    def __init__(self, x, y, weight):
        self.x = x
        self.y = y
        self.weight = weight

    @staticmethod
    def distance_between_two_points(point_one, point_two):
        return math.sqrt(
            (point_one.x - point_two.x) ** 2 + (point_one.y - point_two.y) ** 2)
